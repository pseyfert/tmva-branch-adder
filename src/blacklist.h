#pragma once
#include "TString.h"
#if __cplusplus >= 201103L

bool blacklisted(TString checkme) ;
#endif /// __cplusplus >= 201103L
